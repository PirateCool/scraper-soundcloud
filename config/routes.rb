Rails.application.routes.draw do
  resources :lists
  get 'home/home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#home"
  resources :valid_followers 
  
  resources :channels do
  	resources :valid_followers
  end

end

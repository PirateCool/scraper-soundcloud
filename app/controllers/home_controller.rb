class HomeController < ApplicationController
  def home

		
	@channel = Channel.new(channel_params)
	return new_channel_path


	@allusers = Channel.all.each do |channel|
		@allusers += channel.follower_count 
	end

		

  	if @channel.save
  		times_scrolling = (@channel.follower_count.to_i / 22)
  		@estimate_time = Time.at(2*times_scrolling * 1.2).utc.strftime("%H:%M:%S") + (@channel.follower_count * 1.2)
  	end

  end


  	private 

	def channel_params
      params.permit(:name, :url, :follower_count)
    end

end

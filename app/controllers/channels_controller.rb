class ChannelsController < ApplicationController

	def new
		@channel = Channel.new
	end

	def create 
		@channel = Channel.new(channel_params)
		@channel = Scrapper.new.perform(@channel.url)
			
		redirect_to root_path, :alert => 'Channel Saved, you can check the new followers'
			
	end

	def index
		@channels = Channel.all
	end

	def show 
		@channel = Channel.find(params[:id])
		respond_to do |format|
      		format.html
     		format.csv { send_data @channels.to_csv, filename: "chan-followers-#{Date.today}.csv" }
     	end
	end

	private 

	def channel_params
      params.require(:channel).permit(:name, :url, :follower_count)
    end

    def channel_url
    	params.require(:channel).permit(:url)
    end

end

	
class ValidFollowersController < ApplicationController








	def show
		@follower = ValidFollower.find(params[:id])
	end

	def index
		@followers = ValidFollower.all

		    respond_to do |format|
      		format.html
     		format.csv { send_data @followers.to_csv, filename: "followers-#{Date.today}.csv" }
    end
	end


	private 

	def valid_follower_params
      params.require(:follower).permit(:username, :handle, :follower_count, :channel_id, :email)
    end





end

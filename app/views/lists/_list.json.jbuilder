json.extract! list, :id, :url, :note, :created_at, :updated_at
json.url list_url(list, format: :json)

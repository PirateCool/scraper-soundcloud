class Scrapper
  require 'open-uri'
      # 1 - Récéption de lurl de la page, perfom 
      # 2 - Initialisation de la page 
      # 3 - Aller sur la page/followers & Watir scroll tout en bas
      # 4 - Scrapper la page scrollée et stocker les followers avec email


  def scrapChannel(page)
        print "Démarrage du scrapping...       \n\n".green

          # document -> variable dans laquelle on stock l'url dans Nogokiri
          # page -> parametre de la méthode -> URL soundcloud a entrer pour scrapper
          document = Nokogiri::HTML.parse(open(page))

          # on défini la variable channel en hash
          channel = {}
          # Scraping & stockage des clés valeur dans channel
          channel[:username] = document.css("h1").text
          channel[:link] = Marshal.load(Marshal.dump(page))
          channel[:emails] = document.at('meta[name="description"]')['content'].scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i)
          channel[:followers_count] = document.at('meta[property="soundcloud:follower_count"]')['content']
          handle = channel[:link].sub!("https://soundcloud.com/", "@")
          handle.gsub!("/", "") 
          channel[:handle] = handle



          if channel[:emails][0] == nil
            channel[:emails][0] = ""
            puts "\n Channel : #{channel[:username].yellow} (#{handle}) - #{channel[:followers_count].yellow} followers - aucun email trouvé.\n\n"
          else    
            puts "\n Channel : #{channel[:username].yellow} (#{handle}) - #{channel[:followers_count].yellow} followers - emails : #{channel[:emails].join(", ").green}\n\n"
          end
          # channel a maintenant les infos -> username, link, email, follower_count & email (si present)
          channel.inspect

          # On lance la methode suivante
          @channel = Channel.create(name: channel[:username], url: channel[:link], follower_count: channel[:followers_count])
          channel = @channel
          anti_lazy_load(page)

  end



  def anti_lazy_load(page)
      # ANTI LAZY LOADING sur la page Followers
      # Pour afficher tout les follower d'une page sur soundcloud.com/page/followers
      # Il faut scroll toutes les 1,2 secondes 
      # A chaque fois, soundcloud génère 22 nouveaux followers


               # 1 - times_scrolling
               # On peut donc savoir combien de fois il faut attendre puis scroller
               # En divisant le nombre de follower de la page a scroller par 22

              times_scrolling = (@channel.follower_count.to_i / 22)

                              # UX 
                              estimate_time = Time.at(times_scrolling * 1.2).utc.strftime("%H:%M:%S")
                              puts "Temps de scroll éstimé : #{estimate_time}\n\n"
                              # UX (pourcentage)
                              percentage = 0.0
                              zero_to_cent = (percentage / times_scrolling * 100)
        
        # 2 - Watir 
        # On lance watir pour scroller toute la page et ainsi afficher tous les éléments
              @browser = Watir::Browser.new :firefox, headless: true, marionette: true
              @browser.goto (page + '/followers')
          
              # times_scrolling -> nombre calculé
              times_scrolling.times do

                            # UX (pourcentage)
                            percentage += 22 
                            print "#{"Scrapping :".yellow} #{zero_to_cent}#{"%"}  \r"
                                                                                  # to_i ?      
              # Watir -> Scroll en bas             
              @browser.scroll.to :bottom
              # Attend 1.2 secondes 
              sleep(1.2)
              # Relance nombre de fois calculé
          end # --> TOUTE LA PAGE A ETE SCROLLE ET EST PRETE A ETRE SCRAPPER !

          # Lance valid_follower avec la page scrollée
          valid_follower(@browser.html)
                    @browser.close

  end # fin antilazy loading


  def valid_follower(page)
    # =>            NOGOKIRI VALID FOLLOWERS 
    # 1 - Scrapping de la page scrollée par Watir
    # document = Nogokiri sur la Page totalement scrolléé (browser)
      document = Nokogiri::HTML(page)

    # Récupère tous les href des followers d'une page (selecteur userBadgeList + href)
      all_channel_followers = document.css('a.userBadgeListItem__heading').map { |link| link['href'] }


         # followers = Tout les followers VALIDE
        followers = []

        # Pour chacun des href dans page/followers
            all_channel_followers.each do |follower|

              user = scrapFollower("https://soundcloud.com" + follower)
                  # user = Session du scrapFollower avec son href 
              handle = user[:link].sub!("https://soundcloud.com/", "@")
              handle.gsub!("/", "") # handle soundcloud du follower = "url du follower" + "@""

                  # Si l'utilisateur a 0 email, il n'est pas valide
                  if user[:emails][0] == nil
                    puts "#{@channel.name} Follower: #{user[:username].yellow} (#{handle}) - #{user[:followers_count].yellow} followers - Aucun email trouvé."
                  
                  else # Si l'utilisateur a un ou plusieurs emails, il est valide!
                    puts "#{@channel.name} Follower: #{user[:username].yellow} (#{handle}) - #{user[:followers_count].yellow} followers - Emails sont : #{user[:emails].join(", ").green}"
                    # On le stock dans followers
                    followers << user # followers --> FOLLOWERS VALIDES !!
                    ValidFollower.create(channel_id: @channel.id, email: user[:emails], follower_count: user[:followers_count], handle: user[:link])
                  end
            # Relance pour chaque follower
            end
              followers.inspect
      # RETURN FOLLOWERS ? 

       # On ferme la page a scrape (Watir)
  end 


# Scraper pour un follower
  def scrapFollower(page)
    # document =  Nogokiri sur la page du follower 
    document = Nokogiri::HTML.parse(open(page))
    # Créer le hash user
    user = {}
    # Scraping et stockage des clés - valeurs dans user
    user[:username] = document.css("h1").text
    user[:link] = page
    user[:followers_count] = document.at('meta[property="soundcloud:follower_count"]')['content']
    user[:emails] = document.at('meta[name="description"]')['content'].scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i)
    
    return user
    # => 
  end




  
  def perform(page)
      # print "Entrez le lien du profil Soundcloud concerné :\n> ".yellow
      # Parametre pour scrapper
      scrapChannel(page)
      #save_data_in_csv
  end


end

class CreateValidFollowers < ActiveRecord::Migration[5.2]
  def change
    create_table :valid_followers do |t|
      t.string :username
      t.string :handle
      t.integer :follower_count
      t.string :email
      t.references :channel, foreign_key: true

      t.timestamps
    end
  end
end

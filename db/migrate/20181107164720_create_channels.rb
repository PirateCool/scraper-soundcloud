class CreateChannels < ActiveRecord::Migration[5.2]
  def change
    create_table :channels do |t|
      t.string :name
      t.string :url
      t.integer :follower_count
      t.integer :email
      
      t.timestamps
    end
  end
end
